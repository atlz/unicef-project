export const geoJsonSample = {
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [[100.0, 0.0], [101.0, 0.0], [101.0, 1.0],
          [100.0, 1.0], [100.0, 0.0]]
        ]

      },
      "properties": {
        "id": "id0",
        "prop1": "prop1"
      }
    }
  ]
};

export const geoJsonCities = {
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [[100.0, 0.0], [101.0, 0.0], [101.0, 1.0],
          [100.0, 1.0], [100.0, 0.0]]
        ]

      },
      "properties": {
        "adm_level": "city",
        "region_id": "1",
        "region_name": "Norte",
        "region_abbreviation": "N",
        "state_id": "11",
        "state_name": "Rondônia",
        "state_abbreviation": "RO",
        "city_id": "1100015",
        "city_name": "Alta Floresta D'Oeste",
        "country_id": "BR",
        "country_name": "Brasil"
      }
    }, {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [[100.0, 0.0], [101.0, 0.0], [101.0, 1.0],
          [100.0, 1.0], [100.0, 0.0]]
        ]

      },
      "properties": {
        "adm_level": "city",
        "region_id": "1",
        "region_name": "Norte",
        "region_abbreviation": "N",
        "state_id": "12",
        "state_name": "Acre",
        "state_abbreviation": "AC",
        "city_id": "1200013",
        "city_name": "Acrelândia",
        "country_id": "BR",
        "country_name": "Brasil"
      }
    }
  ]
};

export const geoJsonRegions = {
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [[100.0, 0.0], [101.0, 0.0], [101.0, 1.0],
          [100.0, 1.0], [100.0, 0.0]]
        ]

      },
      "properties": {
        "adm_level": "region",
        "region_id": "1",
        "region_name": "Norte",
        "region_abbreviation": "N",
        "country_id": "BR",
        "country_name": "Brasil"
      }
    }, {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [[100.0, 0.0], [101.0, 0.0], [101.0, 1.0],
          [100.0, 1.0], [100.0, 0.0]]
        ]

      },
      "properties": {
        "adm_level": "region",
        "region_id": "2",
        "region_name": "Nordeste",
        "region_abbreviation": "NE",
        "country_id": "BR",
        "country_name": "Brasil"
      }
    }
  ]
};

export const geoJsonStates = {
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [[100.0, 0.0], [101.0, 0.0], [101.0, 1.0],
          [100.0, 1.0], [100.0, 0.0]]
        ]

      },
      "properties": {
        "adm_level": "state",
        "region_id": "1",
        "region_name": "Norte",
        "region_abbreviation": "N",
        "state_id": "11",
        "state_name": "Rondônia",
        "state_abbreviation": "RO",
        "country_id": "BR",
        "country_name": "Brasil"
      }
    }, {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [[100.0, 0.0], [101.0, 0.0], [101.0, 1.0],
          [100.0, 1.0], [100.0, 0.0]]
        ]

      },
      "properties": {
        "adm_level": "state",
        "region_id": "1",
        "region_name": "Norte",
        "region_abbreviation": "N",
        "state_id": "12",
        "state_name": "Acre",
        "state_abbreviation": "AC",
        "country_id": "BR",
        "country_name": "Brasil"
      }
    }
  ]
};