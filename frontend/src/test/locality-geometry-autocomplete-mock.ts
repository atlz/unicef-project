export const localityGeometryAutocompleteResponseFromServer = {
  "country_id": "BR",
  "country_name": "Brasil",
  "region_id": "5",
  "region_name": "Centro-Oeste",
  "state_id": "51",
  "state_name": "Mato Grosso",
  "state_abbreviation": "MT",
  "city_id": "5106000",
  "city_name": "Nortelândia",
  "id": "5106000",
  "name": "Nortelândia",
  "adm_level": "city"
};

export const localitiesGeometryAutocompleteResponseFromServer = [{
  "country_id": "BR",
  "country_name": "Brasil",
  "region_id": "5",
  "region_name": "Centro-Oeste",
  "state_id": "51",
  "state_name": "Mato Grosso",
  "state_abbreviation": "MT",
  "city_id": "5106000",
  "city_name": "Nortelândia",
  "id": "5106000",
  "name": "Nortelândia",
  "adm_level": "city"
}];