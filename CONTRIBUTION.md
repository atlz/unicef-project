Unicef Connectivity Contribution Guidelines

Thank you for your interest in contributing to a UNICEF project! UNICEF believes in new approaches, partnerships, and technologies that support realizing children's rights are critical to improving their lives. 

Contribute what?

There are different ways to contribute to a project. Some ways to contribute are…

- Writing code
- Submitting bug reports / feature requests
- Improving documentation
- Writing tutorials / blog posts

Getting started
    Hint: Working on your first pull request? Learn how from this free series. How to Contribute to an Open Source Project on GitHub

    1. Fork the project to your GitHub account.
    2. Create changes in your fork.
    3. Send a pull request to our repo.

Report a bug

When filing an issue, answer these five questions:

1. What version of NodeJS/npm are you using (npm version)?
2. What operating system are you using?
3. What did you do?
4. What did you expect to see?
5. What did you see instead?

Code review process

The core team at the UNICEF Office of Innovation reviews pull requests on a weekly basis. The core team may vary – see an updated list in the project index.

Expect a response on new pull requests within five business days (Mon-Fri). If you don't receive any feedback, please follow up with a new comment!

Community

On going definition